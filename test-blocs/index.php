<?php

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
//
?>
<?php $APPLICATION->IncludeComponent(
    'custom:soft',
    '.default',
    [
        'SEF_FOLDER'        => '/test-blocs/',
        'SEF_MODE'          => 'Y',
        'SEF_URL_TEMPLATES' => [
            'list'   => '',
            'detail' => '#ELEMENT_CODE#/',
        ],
        'VARIABLE_ALIASES'  => [],
    ]
);
?>

<?php require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
