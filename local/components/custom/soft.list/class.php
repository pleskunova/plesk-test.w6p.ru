<?php

use Bitrix\Iblock\IblockTable;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;

/**
 * Class SoftList
 */
class SoftList extends CBitrixComponent
{
    public $iblockId = 18;

    /**
     * Function calls __includeComponent in order to execute the component.
     *
     * @throws LoaderException
     */
    public function executeComponent()
    {
        Loader::IncludeModule('iblock');

       // $this->arResult['ITEMS'] = $this->getList();
        $this->arResult['ITEMS'] = $this->getListORM();
        $this->includeComponentTemplate();
    }

    private function getList():array
    {
        $result   = [];
        $arSort   = [
            'SORT' => 'ASC',
        ];
        $arFilter = [
            'IBLOCK_ID' => $this->iblockId,
        ];
        $arSelect = [
            'ID',
            'NAME',
            'CODE',
            'lOGO',
            'DETAIL_PAGE_URL',
        ];
        $res      = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
        while ($ob = $res->GetNextElement()) {
            $arFields     = $ob->GetFields();
            $arProperties = $this->getProperties($arFields['ID']);
            $result[]     = [
                'NAME'            => $arFields['NAME'],
                'CODE'            => $arFields['CODE'],
                'LOGO'            => $arProperties['LOGO'],
                'DETAIL_PAGE_URL' => $arFields['DETAIL_PAGE_URL'],
            ];
        }

        return $result;
    }

    /**
     * @param $elementId
     *
     * @return array
     */
    private function getProperties($elementId):array
    {
        $result     = [];
        $properties = CIBlockElement::GetProperty($this->iblockId, $elementId, ['sort' => 'asc'], ['CODE' => 'LOGO']);
        while ($propFields = $properties->GetNext()) {
            $file   = CFile::ResizeImageGet($propFields['VALUE'], ['width' => 200, 'height' => 200], BX_RESIZE_IMAGE_PROPORTIONAL, true);
            $result = [
                'LOGO' => $file['src'],
            ];
        }

        return $result;
    }

    /**
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function getListORM(): array
    {
        $resultArray = [];
        $entity      = IblockTable::compileEntity('Soft');
        $result      = $entity->getDataClass()::query()
            ->addSelect('ID')
            ->addSelect('NAME')
            ->addSelect('CODE')
            ->addSelect('LOGO.FILE')
            ->where('IBLOCK_ID', $this->iblockId)
            ->setOrder(["ID" => "ASC"])
            ->fetchCollection();
        foreach ($result as $item) {
            $file = $item->get('LOGO')->get('FILE');
            $img = CFile::ResizeImageGet(
                $file->collectValues(),
                [
                    'width'  => 50,
                    'height' => 50,
                ],
                BX_RESIZE_IMAGE_PROPORTIONAL,
                true
            );
            $resultArray[] = [
                'NAME'            => $item->get('NAME'),
                'LOGO'            => $img['src'],
                'DETAIL_PAGE_URL' => $item->get('CODE'),
            ];
        }
        return $resultArray;
    }

}
