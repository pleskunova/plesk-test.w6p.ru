<?php

use Bitrix\Highloadblock as HL;
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Iblock\IblockTable;
use Bitrix\Main\Entity;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\SystemException;

/**
 * Class Soft
 */
class SoftDetail extends CBitrixComponent
{

    private $code;
    private $iblockId;

    /**
     * @throws LoaderException|SystemException
     */
    public function executeComponent()
    {
        $this->iblockId = $this->arParams['IBLOCK_ID'];
        $this->code     = $this->arParams['ELEMENT_CODE'];

        Loader::IncludeModule('iblock');
        Loader::IncludeModule('highloadblock');

        // $this->arResult['ITEMS'] = $this->getList();
        $this->arResult['ITEMS'] = $this->getListORM();
        $this->includeComponentTemplate();
    }

    /**
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\ArgumentException
     * @throws SystemException
     */

    /**
     * @throws SystemException
     */
    private function getList():array
    {
        $result   = [];
        $arSort   = [
            'SORT' => 'ASC',
        ];
        $arFilter = [
            'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
            'CODE'      => $this->arParams['ELEMENT_CODE'],
        ];
        $arSelect = [
            'ID',
            'NAME',
            'LOGO',
            'DETAIL_TEXT',

        ];
        $res      = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);

        while ($ob = $res->GetNextElement()) {
            $arFields     = $ob->GetFields();
            $arProperties = $this->getProperties($arFields['ID']);
            $arNameOS     = $this->getNameDictionary($arProperties['OS']['VALUE']);

            $result[] = [
                'NAME'        => $arFields['NAME'],
                'LOGO'        => $arProperties['LOGO']['VALUE'],
                'DETAIL_TEXT' => $arFields['DETAIL_TEXT'],
                'VERSION'     => $arProperties['VERSION']['VALUE'],
                'OS'          => $arNameOS,

            ];
        }

        return $result;
    }

    /**
     * @param $elementId
     *
     * @return array
     */
    private function getProperties($elementId):array
    {
        $result     = [];
        $properties = CIBlockElement::GetProperty($this->arParams['IBLOCK_ID'], $elementId, ['sort' => 'asc']);
        while ($propFields = $properties->GetNext()) {
            $value = $propFields['VALUE'];
            if ($propFields['CODE'] === 'LOGO') {
                $file  = CFile::ResizeImageGet($propFields['VALUE'], ['width' => 250, 'height' => 250], BX_RESIZE_IMAGE_PROPORTIONAL, true);
                $value = $file['src'];
            }

            $result[$propFields['CODE']] = [
                'NAME'  => $propFields['NAME'],
                'VALUE' => $value,
            ];
        }

        return $result;
    }

    /**
     * @param $code
     *
     * @throws SystemException
     */
    private function getNameDictionary($code)
    {
        $result  = [];
        $hlbl    = 3;
        $hlblock = HighloadBlockTable::getById($hlbl)->fetch();

        $entity            = HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();
        $rsData            = $entity_data_class::getList([
            'select' => ['UF_NAME'],
            'order'  => ['UF_NAME' => 'ASC'],
            'filter' => ['UF_XML_ID' => $code],
        ]);

        while ($arData = $rsData->Fetch()) {
            $result =
                [
                    'OS' => $arData['UF_NAME'],
                ];
        }

        return $result['OS'];
    }


    /**
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\ArgumentException
     * @throws SystemException
     */
    public function getListORM():array
    {
        $entity = IblockTable::compileEntity('Soft');
        $result = $entity->getDataClass()::query()
            ->addSelect('ID')
            ->addSelect('NAME')
            ->addSelect('CODE')
            ->addSelect('LOGO.FILE')
            ->addSelect('DETAIL_TEXT')
            ->addSelect('VERSION.ITEM')
            ->addSelect('OS')
            ->where('IBLOCK_ID', $this->iblockId)
            ->where('CODE', $this->code)
            ->fetchCollection();

        foreach ($result as $item) {
            $osID   = $item->get('OS')->get('VALUE');
            $osName = $this->getOs($osID);
            $version  = $item->get('VERSION');
            foreach ($version as $value) {
                $versionOs = $value->get('ITEM')->collectValues();
                //var_dump($V);
            }

            $file = $item->get('LOGO')->get('FILE');

            $img = CFile::ResizeImageGet(
                $file->collectValues(),
                [
                    'width'  => 300,
                    'height' => 300,
                ],
                BX_RESIZE_IMAGE_PROPORTIONAL,
                true
            );

            $res[] = [
                'NAME'        => $item->get('NAME'),
                'DETAIL_TEXT' => $item->get('DETAIL_TEXT'),
                'LOGO'        => $img['src'],
                'VERSION'     => $versionOs['VALUE'],
                'OS'          => $osName['UF_NAME'],
            ];
        }

        return $res;
    }

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getOs($osID)
    {
        $hlbl    = 3;
        $hlblock = HighloadBlockTable::getById($hlbl)->fetch();
        $entity  = HighloadBlockTable::compileEntity($hlblock);

        return $entity->getDataClass()::query()
            ->addSelect('UF_NAME')
            ->where('UF_XML_ID', $osID)
            ->fetch();
    }
}
