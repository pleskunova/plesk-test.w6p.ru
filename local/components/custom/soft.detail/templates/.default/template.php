
<?php foreach ($arResult['ITEMS'] as $arItem): ?>
    <div class="soft__title-block">
        <h3 class="soft__title"><?=$arItem['NAME']?></h3>
        <p class="soft_img"><img src="<?=$arItem['LOGO']?>" alt=""></p>
        <p class="soft__detail-text"><?=$arItem['DETAIL_TEXT']?></p>
        <p class="soft__detail-text">Версия:<?=$arItem['VERSION']?></p>
        <p class="soft__detail-text">OS:<?=$arItem['OS']?></p>
    </div>
<?php endforeach; ?>
