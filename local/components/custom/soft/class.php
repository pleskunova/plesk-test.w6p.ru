<?php

CModule::IncludeModule('iblock');

/**
 * Class Soft
 */
class Soft extends CBitrixComponent
{
    public function executeComponent()
    {
        $arParams = $this->arParams;

        $arDefaultUrlTemplates = [
            'list'   => 'index.php',
            'detail' => '#ELEMENT_CODE#'
        ];

        $arDefaultVariableAliases404 = [];
        $arDefaultVariableAliases    = [];
        $arComponentVariables        = ['ELEMENT_CODE'];
        $SEF_FOLDER                  = '';
        $arUrlTemplates              = [];

        if ($arParams['SEF_MODE']) {
            $arVariables = [];

            $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates(
                $arDefaultUrlTemplates,
                $arParams['SEF_URL_TEMPLATES']
            );

            $arVariableAliases = CComponentEngine::MakeComponentVariableAliases(
                $arDefaultVariableAliases404,
                $arParams['VARIABLE_ALIASES']
            );

            $componentPage = CComponentEngine::ParseComponentPath(
                $arParams['SEF_FOLDER'],
                $arUrlTemplates,
                $arVariables
            );

            if (strlen($componentPage) <= 0) {
                $componentPage = 'list';
            }

            CComponentEngine::InitComponentVariables(
                $componentPage,
                $arComponentVariables,
                $arVariableAliases,
                $arVariables
            );

            $SEF_FOLDER = $arParams['SEF_FOLDER'];
        } else {
            $arVariables = [];

            $arVariableAliases = CComponentEngine::MakeComponentVariableAliases(
                $arDefaultVariableAliases,
                $arParams['VARIABLE_ALIASES']
            );

            CComponentEngine::InitComponentVariables(
                false,
                $arComponentVariables,
                $arVariableAliases,
                $arVariables
            );

            $componentPage = '';

            if (trim($arVariables['ELEMENT_CODE']) !== '') {
                $componentPage = 'detail';
            } else {
                $componentPage = 'list';
            }
        }

        $this->arResult = [
            'FOLDER'        => $SEF_FOLDER,
            'URL_TEMPLATES' => $arUrlTemplates,
            'VARIABLES'     => $arVariables,
            'ALIASES'       => $arVariableAliases,
        ];

        $this->IncludeComponentTemplate($componentPage);
    }



}
